import React, { Component } from "react";
import Room from "./Room";
class RoomsContainer extends Component {
  state = {
    abs: "adasd",
    rooms: [
      {
        roomId: 1,
        description: "desc1",
        admin: "admin1",
        topics: [
          { topicId: 1, message: "message", votes: 0 },
          { topicId: 2, message: "message2", votes: 0 },
          { topicId: 3, message: "message3", votes: 0 }
        ]
      },
      {
        roomId: 2,
        description: "desc2",
        admin: "admin2",
        topics: [
          { topicId: 1, message: "message21", votes: 0 },
          { topicId: 2, message: "message22", votes: 0 },
          { topicId: 3, message: "message23", votes: 0 }
        ]
      }
    ]
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Admin</th>
              <th scope="col">Description</th>
              <th scope="col">Join</th>
            </tr>
          </thead>
          <tbody>
            {this.state.rooms.length > 0 &&
              this.state.rooms.map(room => (
                <Room key={room.roomId} roomInfo={room} />
              ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default RoomsContainer;
