import React, { Component } from "react";
import MessagesContainer from "./MessagesContainer";
class Room extends Component {
  state = {
    selectedRoom: null
  };

  handleJoinRoom = room => {
    this.setState({ selectedRoom: room });
  };

  render() {
    const { description, admin, roomId ,topics} = this.props.roomInfo;
    return (
      <>
        {!this.state.selectedRoom && (
          <tr>
            <th scope="row">{roomId}</th>
            <td>{admin}</td>
            <td>{description}</td>
            <td>
              <button
                onClick={() => this.handleJoinRoom(this.props.roomInfo)}
                className="btn btn-secondary btn-sm"
              >
                Join
              </button>
            </td>
          </tr>
        )}
        {this.state.selectedRoom && <MessagesContainer topisc={topics}/>}
      </>
    );
  }
}

export default Room;
