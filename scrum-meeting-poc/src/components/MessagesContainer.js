import React, { Component } from "react";
import Message from "./Message";

class MessagesContainer extends Component {
  state = {};
  render() {
    return (
      <ul>
        {this.props.topisc.length > 0 &&
          this.props.topisc.map(message => <Message message={message} />)}
      </ul>
    );
  }
}

export default MessagesContainer;
